﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OAuthProvider
{
    public abstract class SocialMediaAuthenticationProviderBase
    {
        protected string _appId { get; set; }
        protected string _secretKey { get; set; }
        protected string _callbackUrl { get; set; }
        public SocialMediaAuthenticationProviderBase(string appId, string secretKey, string callbackUrl)
        {
            _appId = appId;
            _secretKey = secretKey;
            _callbackUrl = callbackUrl;
        }
        public abstract string GetRedirectUrl();
        public abstract OAuthUserInfo GetUserInfo();
        protected string BuildQuery(Dictionary<string, string> queries)
        {
            if (queries == null || queries.Count == 0) { return String.Empty; }
            StringBuilder sb = new StringBuilder();
            sb.Append("?");
            foreach (var q in queries)
            {
                sb.AppendFormat("{0}={1}&", q.Key, q.Value);
            }
            return sb.ToString().TrimEnd('&');
        }
        protected string MakeHttpGetRequest(string url)
        {
            using (var http = new HttpClient())
            {
                var response = http.GetAsync(url).GetAwaiter().GetResult();
                return response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            }
        }
        protected string MakeHttpPostRequest(string url, IEnumerable<KeyValuePair<string, string>> body)
        {
            using (var http = new HttpClient())
            {
                var content = new FormUrlEncodedContent(body);
                var result = http.PostAsync(url, content).GetAwaiter().GetResult();
                return result.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            }
        }
        protected bool IsAntiForgeryTokenValid(string key)
        {
            return HttpContext.Current.Request["state"] != null && HttpContext.Current.Session[key] != null 
                                                                && HttpContext.Current.Request["state"].ToString() == HttpContext.Current.Session[key].ToString();
        }
    }
}
