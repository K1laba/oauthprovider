﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OAuthProvider
{ 
    public class GoogleAuthenticationProvider : SocialMediaAuthenticationProviderBase
    {
        private const string _authUrl = "https://accounts.google.com/o/oauth2/auth";
        private const string _profileUrl = "https://www.googleapis.com/oauth2/v1/userinfo";
        private const string _tokenUrl = "https://www.googleapis.com/oauth2/v3/token";
        private const string _stateKey = "googlestate";
        public GoogleAuthenticationProvider(string appId, string secretKey, string callbackUrl) : base(appId, secretKey, callbackUrl) { }
        public override string GetRedirectUrl()
        {
            string googleState = new Guid().ToString();
            HttpContext.Current.Session[_stateKey] = googleState;
            Dictionary<string, string> queries = new Dictionary<string, string>();
            queries.Add("client_id", _appId);
            queries.Add("response_type", "code");
            queries.Add("scope", "openid%20email");
            queries.Add("redirect_uri", _callbackUrl);
            queries.Add("state", googleState);
            return _authUrl + base.BuildQuery(queries);
        }

        public override OAuthUserInfo GetUserInfo()
        {
            if (!base.IsAntiForgeryTokenValid(_stateKey)) { return null; }
            string token = GetToken();
            
            string content = RequestUserInfo(GetToken());
            dynamic userInfo = Newtonsoft.Json.JsonConvert.DeserializeObject(content);
            return new OAuthUserInfo()
            {
                FirstName = userInfo.given_name,
                LastName = userInfo.family_name,
                Email = userInfo.email,
                GoogleId = userInfo.id
            };
        }

        private string GetToken()
        {
            var body = new Dictionary<string, string>();
            body.Add("code", HttpContext.Current.Request["code"]);
            body.Add("client_id", _appId);
            body.Add("client_secret", _secretKey);
            body.Add("redirect_uri", _callbackUrl);
            body.Add("grant_type", "authorization_code");

            string content = base.MakeHttpPostRequest(_tokenUrl, body);

            dynamic tokenInfo = Newtonsoft.Json.JsonConvert.DeserializeObject(content);
            return tokenInfo.access_token.ToString();
        }
        private string RequestUserInfo(string token)
        {
            var queries = new Dictionary<string, string>();
            queries.Add("alt", "json");
            queries.Add("access_token", token);

           return base.MakeHttpGetRequest(_profileUrl + base.BuildQuery(queries));
        }
    }
}
