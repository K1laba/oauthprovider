﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OAuthProvider
{
    public class FacebookAuthenticationProvider : SocialMediaAuthenticationProviderBase
    {
        private const string _authUrl = "https://www.facebook.com/v2.8/dialog/oauth";
        private const string _profileInfoUrl = "https://graph.facebook.com/me";
        private const string _tokenRequestUrl = "https://graph.facebook.com";
        private const string _stateKey = "fbstate";

        public FacebookAuthenticationProvider(string appId, string secretKey, string callbackUrl) : base(appId, secretKey, callbackUrl) { }
        public override string GetRedirectUrl()
        {
            string state = Guid.NewGuid().ToString();
            HttpContext.Current.Session[_stateKey] = state;

            Dictionary<string, string> queries = new Dictionary<string, string>();
            queries.Add("app_id", _appId);
            queries.Add("response_type", "code");
            queries.Add("scope", "email");
            queries.Add("redirect_uri", _callbackUrl);
            queries.Add("state", state);
            return _authUrl + base.BuildQuery(queries);
        }

        public override OAuthUserInfo GetUserInfo()
        {
            if (!base.IsAntiForgeryTokenValid(_stateKey)) { return null; }
            Dictionary<string, string> queries = new Dictionary<string, string>();
            queries.Add("access_token", this.GetToken());
            queries.Add("fields", "first_name,last_name,id,email");

            string content = base.MakeHttpGetRequest(_profileInfoUrl + base.BuildQuery(queries));
            dynamic user = JsonConvert.DeserializeObject(content);
            return new OAuthUserInfo()
            {
                FacebookId = user.id,
                Email = user.email,
                FirstName = user.first_name,
                LastName = user.last_name,
            };
        }
        private string GetToken()
        {
            try
            {
                var queries = new Dictionary<string, string>();
                queries.Add("code", HttpContext.Current.Request["code"]);
                queries.Add("client_id", _appId);
                queries.Add("client_secret", _secretKey);
                queries.Add("redirect_uri", _callbackUrl);

                var content = base.MakeHttpGetRequest(_tokenRequestUrl + "/v2.8/oauth/access_token" + base.BuildQuery(queries));
                dynamic tokenInfo = JsonConvert.DeserializeObject(content);
                return tokenInfo.access_token.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
